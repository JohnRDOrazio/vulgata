# Lamentationes Jeremiæ Prophetæ {#Lamentationes}

## 1

[Prologus.]{style="font-variant:small-caps"} Et factum est, postquam in captivitatem redactus est Israël, et Jerusalem deserta est, sedit Jeremias propheta flens, et planxit lamentatione hac in Jerusalem : et amaro animo suspirans et ejulans, dixit : 
[Aleph.]{style="font-variant:small-caps"} Quomodo sedet sola\
 civitas plena populo !\
 Facta est quasi vidua\
 domina gentium ;\
 princeps provinciarum\
 facta est sub tributo.\

^2^ [Beth.]{style="font-variant:small-caps"} Plorans ploravit in nocte,\
 et lacrimæ ejus in maxillis ejus :\
 non est qui consoletur eam\
 ex omnibus caris ejus ;\
 omnes amici ejus spreverunt eam,\
 et facti sunt ei inimici.\

^3^ [Ghimel.]{style="font-variant:small-caps"} Migravit Judas propter afflictionem,\
 et multitudinem servitutis ;\
 habitavit inter gentes,\
 nec invenit requiem :\
 omnes persecutores ejus apprehenderunt eam\
 inter angustias.\

^4^ [Daleth.]{style="font-variant:small-caps"} Viæ Sion lugent, eo quod non sint\
 qui veniant ad solemnitatem :\
 omnes portæ ejus destructæ,\
 sacerdotes ejus gementes ;\
 virgines ejus squalidæ,\
 et ipsa oppressa amaritudine.\

^5^ [He.]{style="font-variant:small-caps"} Facti sunt hostes ejus in capite ;\
 inimici ejus locupletati sunt :\
 quia Dominus locutus est super eam\
 propter multitudinem iniquitatum ejus.\
 Parvuli ejus ducti sunt in captivitatem\
 ante faciem tribulantis.\

^6^ [Vau.]{style="font-variant:small-caps"} Et egressus est a filia Sion\
 omnis decor ejus ;\
 facti sunt principes ejus velut arietes\
 non invenientes pascua,\
 et abierunt absque fortitudine\
 ante faciem subsequentis.\

^7^ [Zain.]{style="font-variant:small-caps"} Recordata est Jerusalem dierum afflictionis suæ,\
 et prævaricationis,\
 omnium desiderabilium suorum,\
 quæ habuerat a diebus antiquis,\
 cum caderet populus ejus in manu hostili,\
 et non esset auxiliator :\
 viderunt eam hostes,\
 et deriserunt sabbata ejus.\

^8^ [Heth.]{style="font-variant:small-caps"} Peccatum peccavit Jerusalem,\
 propterea instabilis facta est ;\
 omnes qui glorificabant eam spreverunt illam,\
 quia viderunt ignominiam ejus :\
 ipsa autem gemens\
 conversa est retrorsum.\

^9^ [Teth.]{style="font-variant:small-caps"} Sordes ejus in pedibus ejus,\
 nec recordata est finis sui ;\
 deposita est vehementer,\
 non habens consolatorem.\
 Vide, Domine, afflictionem meam,\
 quoniam erectus est inimicus.\

^10^ [Jod.]{style="font-variant:small-caps"} Manum suam misit hostis\
 ad omnia desiderabilia ejus,\
 quia vidit gentes\
 ingressas sanctuarium suum,\
 de quibus præceperas\
 ne intrarent in ecclesiam tuam.\

^11^ [Caph.]{style="font-variant:small-caps"} Omnis populus ejus gemens,\
 et quærens panem ;\
 dederunt pretiosa quæque pro cibo\
 ad refocillandam animam.\
 Vide, Domine, et considera\
 quoniam facta sum vilis !\

^12^ [Lamed.]{style="font-variant:small-caps"} O vos omnes qui transitis per viam,\
 attendite, et videte\
 si est dolor sicut dolor meus !\
 quoniam vindemiavit me,\
 ut locutus est Dominus,\
 in die iræ furoris sui.\

^13^ [Mem.]{style="font-variant:small-caps"} De excelso misit ignem in ossibus meis,\
 et erudivit me :\
 expandit rete pedibus meis,\
 convertit me retrorsum ;\
 posuit me desolatam,\
 tota die mœrore confectam.\

^14^ [Nun.]{style="font-variant:small-caps"} Vigilavit jugum iniquitatum mearum ;\
 in manu ejus convolutæ sunt,\
 et impositæ collo meo.\
 Infirmata est virtus mea :\
 dedit me Dominus in manu\
 de qua non potero surgere.\

^15^ [Samech.]{style="font-variant:small-caps"} Abstulit omnes magnificos meos Dominus\
 de medio mei ;\
 vocavit adversum me tempus\
 ut contereret electos meos.\
 Torcular calcavit Dominus\
 virgini filiæ Juda.\

^16^ [Ain.]{style="font-variant:small-caps"} Idcirco ego plorans,\
 et oculus meus deducens aquas,\
 quia longe factus est a me consolator,\
 convertens animam meam.\
 Facti sunt filii mei perditi,\
 quoniam invaluit inimicus.\

^17^ [Phe.]{style="font-variant:small-caps"} Expandit Sion manus suas ;\
 non est qui consoletur eam.\
 Mandavit Dominus adversum Jacob\
 in circuitu ejus hostes ejus ;\
 facta est Jerusalem\
 quasi polluta menstruis inter eos.\

^18^ [Sade.]{style="font-variant:small-caps"} Justus est Dominus,\
 quia os ejus ad iracundiam provocavi.\
 Audite, obsecro, universi populi,\
 et videte dolorem meum :\
 virgines meæ et juvenes mei abierunt\
 in captivitatem.\

^19^ [Coph.]{style="font-variant:small-caps"} Vocavi amicos meos,\
 et ipsi deceperunt me ;\
 sacerdotes mei et senes mei\
 in urbe consumpti sunt,\
 quia quæsierunt cibum sibi\
 ut refocillarent animam suam.\

^20^ [Res.]{style="font-variant:small-caps"} Vide, Domine, quoniam tribulor :\
 conturbatus est venter meus,\
 subversum est cor meum in memetipsa,\
 quoniam amaritudine plena sum.\
 Foris interfecit gladius,\
 et domi mors similis est.\

^21^ [Sin.]{style="font-variant:small-caps"} Audierunt quia ingemisco ego,\
 et non est qui consoletur me ;\
 omnes inimici mei audierunt malum meum,\
 lætati sunt quoniam tu fecisti :\
 adduxisti diem consolationis,\
 et fient similes mei.\

^22^ [Thau.]{style="font-variant:small-caps"} Ingrediatur omne malum eorum coram te :\
 et vindemia eos, sicut vindemiasti me\
 propter omnes iniquitates meas :\
 multi enim gemitus mei,\
 et cor meum mœrens.




## 2


[Aleph.]{style="font-variant:small-caps"} Quomodo obtexit caligine in furore suo\
 Dominus filiam Sion ;\
 projecit de cælo in terram\
 inclytam Israël,\
 et non est recordatus scabelli pedum suorum\
 in die furoris sui !\

^2^ [Beth.]{style="font-variant:small-caps"} Præcipitavit Dominus, nec pepercit\
 omnia speciosa Jacob :\
 destruxit in furore suo\
 munitiones virginis Juda,\
 et dejecit in terram ;\
 polluit regnum et principes ejus.\

^3^ [Ghimel.]{style="font-variant:small-caps"} Confregit in ira furoris sui\
 omne cornu Israël ;\
 avertit retrorsum dexteram suam\
 a facie inimici,\
 et succendit in Jacob quasi ignem\
 flammæ devorantis in gyro.\

^4^ [Daleth.]{style="font-variant:small-caps"} Tetendit arcum suum quasi inimicus,\
 firmavit dexteram suam quasi hostis,\
 et occidit omne quod pulchrum erat visu\
 in tabernaculo filiæ Sion ;\
 effudit quasi ignem\
 indignationem suam.\

^5^ [He.]{style="font-variant:small-caps"} Factus est Dominus velut inimicus,\
 præcipitavit Israël :\
 præcipitavit omnia mœnia ejus,\
 dissipavit munitiones ejus,\
 et replevit in filia Juda\
 humiliatum et humiliatam.\

^6^ [Vau.]{style="font-variant:small-caps"} Et dissipavit quasi hortum tentorium suum ;\
 demolitus est tabernaculum suum.\
 Oblivioni tradidit Dominus in Sion\
 festivitatem et sabbatum ;\
 et in opprobrium, et in indignationem furoris sui,\
 regem et sacerdotem.\

^7^ [Zain.]{style="font-variant:small-caps"} Repulit Dominus altare suum ;\
 maledixit sanctificationi suæ :\
 tradidit in manu inimici\
 muros turrium ejus.\
 Vocem dederunt in domo Domini\
 sicut in die solemni.\

^8^ [Heth.]{style="font-variant:small-caps"} Cogitavit Dominus dissipare\
 murum filiæ Sion ;\
 tetendit funiculum suum,\
 et non avertit manum suam a perditione :\
 luxitque antemurale,\
 et murus pariter dissipatus est.\

^9^ [Teth.]{style="font-variant:small-caps"} Defixæ sunt in terra portæ ejus,\
 perdidit et contrivit vectes ejus ;\
 regem ejus et principes ejus in gentibus :\
 non est lex,\
 et prophetæ ejus non invenerunt\
 visionem a Domino.\

^10^ [Jod.]{style="font-variant:small-caps"} Sederunt in terra, conticuerunt\
 senes filiæ Sion ;\
 consperserunt cinere capita sua,\
 accincti sunt ciliciis :\
 abjecerunt in terram capita sua\
 virgines Jerusalem.\

^11^ [Caph.]{style="font-variant:small-caps"} Defecerunt præ lacrimis oculi mei,\
 conturbata sunt viscera mea ;\
 effusum est in terra jecur meum\
 super contritione filiæ populi mei,\
 cum deficeret parvulus et lactens\
 in plateis oppidi.\

^12^ [Lamed.]{style="font-variant:small-caps"} Matribus suis dixerunt :\
 Ubi est triticum et vinum ?\
 cum deficerent quasi vulnerati\
 in plateis civitatis,\
 cum exhalarent animas suas\
 in sinu matrum suarum.\

^13^ [Mem.]{style="font-variant:small-caps"} Cui comparabo te, vel cui assimilabo te,\
 filia Jerusalem ?\
 cui exæquabo te, et consolabor te,\
 virgo, filia Sion ?\
 magna est enim velut mare contritio tua :\
 quis medebitur tui ?\

^14^ [Nun.]{style="font-variant:small-caps"} Prophetæ tui viderunt tibi\
 falsa et stulta ;\
 nec aperiebant iniquitatem tuam,\
 ut te ad pœnitentiam provocarent ;\
 viderunt autem tibi assumptiones falsas,\
 et ejectiones.\

^15^ [Samech.]{style="font-variant:small-caps"} Plauserunt super te manibus\
 omnes transeuntes per viam ;\
 sibilaverunt et moverunt caput suum\
 super filiam Jerusalem :\
 Hæccine est urbs, dicentes, perfecti decoris,\
 gaudium universæ terræ ?\

^16^ [Phe.]{style="font-variant:small-caps"} Aperuerunt super te os suum\
 omnes inimici tui :\
 sibilaverunt, et fremuerunt dentibus,\
 et dixerunt : Devorabimus :\
 en ista est dies quam exspectabamus ;\
 invenimus, vidimus.\

^17^ [Ain.]{style="font-variant:small-caps"} Fecit Dominus quæ cogitavit ;\
 complevit sermonem suum,\
 quem præceperat a diebus antiquis :\
 destruxit, et non pepercit,\
 et lætificavit super te inimicum,\
 et exaltavit cornu hostium tuorum.\

^18^ [Sade.]{style="font-variant:small-caps"} Clamavit cor eorum ad Dominum\
 super muros filiæ Sion :\
 Deduc quasi torrentem lacrimas\
 per diem et noctem ;\
 non des requiem tibi,\
 neque taceat pupilla oculi tui.\

^19^ [Coph.]{style="font-variant:small-caps"} Consurge, lauda in nocte,\
 in principio vigiliarum ;\
 effunde sicut aquam cor tuum\
 ante conspectum Domini :\
 leva ad eum manus tuas\
 pro anima parvulorum tuorum,\
 qui defecerunt in fame\
 in capite omnium compitorum.\

^20^ [Res.]{style="font-variant:small-caps"} Vide, Domine, et considera\
 quem vindemiaveris ita.\
 Ergone comedent mulieres fructum suum,\
 parvulos ad mensuram palmæ ?\
 si occiditur in sanctuario Domini\
 sacerdos et propheta ?\

^21^ [Sin.]{style="font-variant:small-caps"} Jacuerunt in terra foris\
 puer et senex ;\
 virgines meæ et juvenes mei\
 ceciderunt in gladio :\
 interfecisti in die furoris tui,\
 percussisti, nec misertus es.\

^22^ [Thau.]{style="font-variant:small-caps"} Vocasti quasi ad diem solemnem,\
 qui terrerent me de circuitu ;\
 et non fuit in die furoris Domini qui effugeret,\
 et relinqueretur :\
 quos educavi et enutrivi,\
 inimicus meus consumpsit eos.




## 3


[Aleph.]{style="font-variant:small-caps"} Ego vir videns paupertatem meam\
 in virga indignationis ejus.\

^2^ [Aleph.]{style="font-variant:small-caps"} Me minavit, et adduxit in tenebras,\
 et non in lucem.\

^3^ [Aleph.]{style="font-variant:small-caps"} Tantum in me vertit et convertit\
 manum suam tota die.\

^4^ [Beth.]{style="font-variant:small-caps"} Vetustam fecit pellem meam et carnem meam ;\
 contrivit ossa mea.\

^5^ [Beth.]{style="font-variant:small-caps"} Ædificavit in gyro meo, et circumdedit me\
 felle et labore.\

^6^ [Beth.]{style="font-variant:small-caps"} In tenebrosis collocavit me,\
 quasi mortuos sempiternos.\

^7^ [Ghimel.]{style="font-variant:small-caps"} Circumædificavit adversum me, ut non egrediar ;\
 aggravavit compedem meum.\

^8^ [Ghimel.]{style="font-variant:small-caps"} Sed et cum clamavero, et rogavero,\
 exclusit orationem meam.\

^9^ [Ghimel.]{style="font-variant:small-caps"} Conclusit vias meas lapidibus quadris ;\
 semitas meas subvertit.\

^10^ [Daleth.]{style="font-variant:small-caps"} Ursus insidians factus est mihi,\
 leo in absconditis.\

^11^ [Daleth.]{style="font-variant:small-caps"} Semitas meas subvertit, et confregit me ;\
 posuit me desolatam.\

^12^ [Daleth.]{style="font-variant:small-caps"} Tetendit arcum suum, et posuit me\
 quasi signum ad sagittam.\

^13^ [He.]{style="font-variant:small-caps"} Misit in renibus meis\
 filias pharetræ suæ.\

^14^ [He.]{style="font-variant:small-caps"} Factus sum in derisum omni populo meo,\
 canticum eorum tota die.\

^15^ [He.]{style="font-variant:small-caps"} Replevit me amaritudinibus ;\
 inebriavit me absinthio.\

^16^ [Vau.]{style="font-variant:small-caps"} Et fregit ad numerum dentes meos ;\
 cibavit me cinere.\

^17^ [Vau.]{style="font-variant:small-caps"} Et repulsa est a pace anima mea ;\
 oblitus sum bonorum.\

^18^ [Vau.]{style="font-variant:small-caps"} Et dixi : Periit finis meus,\
 et spes mea a Domino.\

^19^ [Zain.]{style="font-variant:small-caps"} Recordare paupertatis, et transgressionis meæ,\
 absinthii et fellis.\

^20^ [Zain.]{style="font-variant:small-caps"} Memoria memor ero, et tabescet\
 in me anima mea.\

^21^ [Zain.]{style="font-variant:small-caps"} Hæc recolens in corde meo,\
 ideo sperabo.\

^22^ [Heth.]{style="font-variant:small-caps"} Misericordiæ Domini, quia non sumus consumpti ;\
 quia non defecerunt miserationes ejus.\

^23^ [Heth.]{style="font-variant:small-caps"} Novi diluculo,\
 multa est fides tua.\

^24^ [Heth.]{style="font-variant:small-caps"} Pars mea Dominus, dixit anima mea ;\
 propterea exspectabo eum.\

^25^ [Teth.]{style="font-variant:small-caps"} Bonus est Dominus sperantibus in eum,\
 animæ quærenti illum.\

^26^ [Teth.]{style="font-variant:small-caps"} Bonum est præstolari cum silentio\
 salutare Dei.\

^27^ [Teth.]{style="font-variant:small-caps"} Bonum est viro cum portaverit jugum\
 ab adolescentia sua.\

^28^ [Jod.]{style="font-variant:small-caps"} Sedebit solitarius, et tacebit,\
 quia levavit super se.\

^29^ [Jod.]{style="font-variant:small-caps"} Ponet in pulvere os suum,\
 si forte sit spes.\

^30^ [Jod.]{style="font-variant:small-caps"} Dabit percutienti se maxillam :\
 saturabitur opprobriis.\

^31^ [Caph.]{style="font-variant:small-caps"} Quia non repellet\
 in sempiternum Dominus.\

^32^ [Caph.]{style="font-variant:small-caps"} Quia si abjecit, et miserebitur,\
 secundum multitudinem misericordiarum suarum.\

^33^ [Caph.]{style="font-variant:small-caps"} Non enim humiliavit ex corde suo\
 et abjecit filios hominum.\

^34^ [Lamed.]{style="font-variant:small-caps"} Ut conteret sub pedibus suis\
 omnes vinctos terræ.\

^35^ [Lamed.]{style="font-variant:small-caps"} Ut declinaret judicium viri\
 in conspectu vultus Altissimi.\

^36^ [Lamed.]{style="font-variant:small-caps"} Ut perverteret hominem in judicio suo ;\
 Dominus ignoravit.\

^37^ [Mem.]{style="font-variant:small-caps"} Quis est iste qui dixit ut fieret,\
 Domino non jubente ?\

^38^ [Mem.]{style="font-variant:small-caps"} Ex ore Altissimi non egredientur\
 nec mala nec bona ?\

^39^ [Mem.]{style="font-variant:small-caps"} Quid murmuravit homo vivens,\
 vir pro peccatis suis ?\

^40^ [Nun.]{style="font-variant:small-caps"} Scrutemur vias nostras, et quæramus,\
 et revertamur ad Dominum.\

^41^ [Nun.]{style="font-variant:small-caps"} Levemus corda nostra cum manibus\
 ad Dominum in cælos.\

^42^ [Nun.]{style="font-variant:small-caps"} Nos inique egimus, et ad iracundiam provocavimus ;\
 idcirco tu inexorabilis es.\

^43^ [Samech.]{style="font-variant:small-caps"} Operuisti in furore, et percussisti nos ;\
 occidisti, nec pepercisti.\

^44^ [Samech.]{style="font-variant:small-caps"} Opposuisti nubem tibi,\
 ne transeat oratio.\

^45^ [Samech.]{style="font-variant:small-caps"} Eradicationem et abjectionem posuisti me\
 in medio populorum.\

^46^ [Phe.]{style="font-variant:small-caps"} Aperuerunt super nos os suum\
 omnes inimici.\

^47^ [Phe.]{style="font-variant:small-caps"} Formido et laqueus facta est nobis\
 vaticinatio, et contritio.\

^48^ [Phe.]{style="font-variant:small-caps"} Divisiones aquarum deduxit oculus meus,\
 in contritione filiæ populi mei.\

^49^ [Ain.]{style="font-variant:small-caps"} Oculus meus afflictus est, nec tacuit,\
 eo quod non esset requies.\

^50^ [Ain.]{style="font-variant:small-caps"} Donec respiceret et videret\
 Dominus de cælis.\

^51^ [Ain.]{style="font-variant:small-caps"} Oculus meus deprædatus est animam meam\
 in cunctis filiabus urbis meæ.\

^52^ [Sade.]{style="font-variant:small-caps"} Venatione ceperunt me quasi avem\
 inimici mei gratis.\

^53^ [Sade.]{style="font-variant:small-caps"} Lapsa est in lacum vita mea,\
 et posuerunt lapidem super me.\

^54^ [Sade.]{style="font-variant:small-caps"} Inundaverunt aquæ super caput meum ;\
 dixi : Perii.\

^55^ [Coph.]{style="font-variant:small-caps"} Invocavi nomen tuum, Domine,\
 de lacu novissimo.\

^56^ [Coph.]{style="font-variant:small-caps"} Vocem meam audisti ; ne avertas aurem tuam\
 a singultu meo et clamoribus.\

^57^ [Coph.]{style="font-variant:small-caps"} Appropinquasti in die quando invocavi te ;\
 dixisti : Ne timeas.\

^58^ [Res.]{style="font-variant:small-caps"} Judicasti, Domine, causam animæ meæ,\
 redemptor vitæ meæ.\

^59^ [Res.]{style="font-variant:small-caps"} Vidisti, Domine, iniquitatem illorum adversum me :\
 judica judicium meum.\

^60^ [Res.]{style="font-variant:small-caps"} Vidisti omnem furorem,\
 universas cogitationes eorum adversum me.\

^61^ [Sin.]{style="font-variant:small-caps"} Audisti opprobrium eorum, Domine,\
 omnes cogitationes eorum adversum me.\

^62^ [Sin.]{style="font-variant:small-caps"} Labia insurgentium mihi, et meditationes eorum\
 adversum me tota die.\

^63^ [Sin.]{style="font-variant:small-caps"} Sessionem eorum et resurrectionem eorum vide ;\
 ego sum psalmus eorum.\

^64^ [Thau.]{style="font-variant:small-caps"} Redes eis vicem, Domine,\
 juxta opera manuum suarum.\

^65^ [Thau.]{style="font-variant:small-caps"} Dabis eis scutum cordis,\
 laborem tuum.\

^66^ [Thau.]{style="font-variant:small-caps"} Persequeris in furore, et conteres eos\
 sub cælis, Domine.




## 4


[Aleph.]{style="font-variant:small-caps"} Quomodo obscuratum est aurum,\
 mutatus est color optimus !\
 dispersi sunt lapides sanctuarii\
 in capite omnium platearum !\

^2^ [Beth.]{style="font-variant:small-caps"} Filii Sion inclyti,\
 et amicti auro primo :\
 quomodo reputati sunt in vasa testea,\
 opus manuum figuli !\

^3^ [Ghimel.]{style="font-variant:small-caps"} Sed et lamiæ nudaverunt mammam,\
 lactaverunt catulos suos :\
 filia populi mei crudelis\
 quasi struthio in deserto.\

^4^ [Daleth.]{style="font-variant:small-caps"} Adhæsit lingua lactentis\
 ad palatum ejus in siti ;\
 parvuli petierunt panem,\
 et non erat qui frangeret eis.\

^5^ [He.]{style="font-variant:small-caps"} Qui vescebantur voluptuose,\
 interierunt in viis ;\
 qui nutriebantur in croceis,\
 amplexati sunt stercora.\

^6^ [Vau.]{style="font-variant:small-caps"} Et major effecta est iniquitas filiæ populi mei\
 peccato Sodomorum,\
 quæ subversa est in momento,\
 et non ceperunt in ea manus.\

^7^ [Zain.]{style="font-variant:small-caps"} Candidiores Nazaræi ejus nive,\
 nitidiores lacte,\
 rubicundiores ebore antiquo,\
 sapphiro pulchriores.\

^8^ [Heth.]{style="font-variant:small-caps"} Denigrata est super carbones facies eorum\
 et non sunt cogniti in plateis ;\
 adhæsit cutis eorum ossibus :\
 aruit, et facta est quasi lignum.\

^9^ [Teth.]{style="font-variant:small-caps"} Melius fuit occisis gladio\
 quam interfectis fame,\
 quoniam isti extabuerunt consumpti\
 a sterilitate terræ.\

^10^ [Jod.]{style="font-variant:small-caps"} Manus mulierum misericordium\
 coxerunt filios suos ;\
 facti sunt cibus earum\
 in contritione filiæ populi mei.\

^11^ [Caph.]{style="font-variant:small-caps"} Complevit Dominus furorem suum,\
 effudit iram indignationis suæ :\
 et succendit ignem in Sion,\
 et devoravit fundamenta ejus.\

^12^ [Lamed.]{style="font-variant:small-caps"} Non crediderunt reges terræ,\
 et universi habitatores orbis,\
 quoniam ingrederetur hostis et inimicus\
 per portas Jerusalem.\

^13^ [Mem.]{style="font-variant:small-caps"} Propter peccata prophetarum ejus,\
 et iniquitates sacerdotum ejus,\
 qui effuderunt in medio ejus\
 sanguinem justorum.\

^14^ [Nun.]{style="font-variant:small-caps"} Erraverunt cæci in plateis,\
 polluti sunt in sanguine ;\
 cumque non possent,\
 tenuerunt lacinias suas.\

^15^ [Samech.]{style="font-variant:small-caps"} Recedite polluti, clamaverunt eis ;\
 recedite, abite, nolite tangere :\
 jurgati quippe sunt, et commoti dixerunt inter gentes :\
 Non addet ultra ut habitet in eis.\

^16^ [Phe.]{style="font-variant:small-caps"} Facies Domini divisit eos,\
 non addet ut respiciat eos ;\
 facies sacerdotum non erubuerunt,\
 neque senum miserti sunt.\

^17^ [Ain.]{style="font-variant:small-caps"} Cum adhuc subsisteremus, defecerunt oculi nostri\
 ad auxilium nostrum vanum ;\
 cum respiceremus attenti ad gentem\
 quæ salvare non poterat.\

^18^ [Sade.]{style="font-variant:small-caps"} Lubricaverunt vestigia nostra\
 in itinere platearum nostrarum ;\
 appropinquavit finis noster, completi sunt dies nostri,\
 quia venit finis noster.\

^19^ [Coph.]{style="font-variant:small-caps"} Velociores fuerunt persecutores nostri\
 aquilis cæli ;\
 super montes persecuti sunt nos,\
 in deserto insidiati sunt nobis.\

^20^ [Res.]{style="font-variant:small-caps"} Spiritus oris nostri, christus Dominus,\
 captus est in peccatis nostris,\
 cui diximus : In umbra tua\
 vivemus in gentibus.\

^21^ [Sin.]{style="font-variant:small-caps"} Gaude et lætare, filia Edom,\
 quæ habitas in terra Hus !\
 ad te quoque perveniet calix : inebriaberis,\
 atque nudaberis.\

^22^ [Thau.]{style="font-variant:small-caps"} Completa est iniquitas tua, filia Sion :\
 non addet ultra ut transmigret te.\
 Visitavit iniquitatem tuam, filia Edom ;\
 discooperuit peccata tua.




## 5


Recordare, Domine, quid acciderit nobis ;\
 intuere et respice opprobrium nostrum.\

^2^ Hæreditas nostra versa est ad alienos,\
 domus nostræ ad extraneos.\

^3^ Pupilli facti sumus absque patre,\
 matres nostræ quasi viduæ.\

^4^ Aquam nostram pecunia bibimus ;\
 ligna nostra pretio comparavimus.\

^5^ Cervicibus nostris minabamur,\
 lassis non dabatur requies.\

^6^ Ægypto dedimus manum et Assyriis,\
 ut saturaremur pane.\

^7^ Patres nostri peccaverunt, et non sunt :\
 et nos iniquitates eorum portavimus.\

^8^ Servi dominati sunt nostri :\
 non fuit qui redimeret de manu eorum.\

^9^ In animabus nostris afferebamus panem nobis,\
 a facie gladii in deserto.\

^10^ Pellis nostra quasi clibanus exusta est,\
 a facie tempestatum famis.\

^11^ Mulieres in Sion humiliaverunt,\
 et virgines in civitatibus Juda.\

^12^ Principes manu suspensi sunt ;\
 facies senum non erubuerunt.\

^13^ Adolescentibus impudice abusi sunt,\
 et pueri in ligno corruerunt.\

^14^ Senes defecerunt de portis,\
 juvenes de choro psallentium.\

^15^ Defecit gaudium cordis nostri ;\
 versus est in luctum chorus noster.\

^16^ Cecidit corona capitis nostri :\
 væ nobis, quia peccavimus !\

^17^ Propterea mœstum factum est cor nostrum ;\
 ideo contenebrati sunt oculi nostri,\

^18^ propter montem Sion quia disperiit ;\
 vulpes ambulaverunt in eo.\

^19^ Tu autem, Domine, in æternum permanebis,\
 solium tuum in generationem et generationem.\

^20^ Quare in perpetuum oblivisceris nostri,\
 derelinques nos in longitudine dierum ?\

^21^ Converte nos, Domine, ad te, et convertemur ;\
 innova dies nostros, sicut a principio.\

^22^ Sed projiciens repulisti nos :\
 iratus es contra nos vehementer.



